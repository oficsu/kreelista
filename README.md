Kreelista Plymouth Theme
=====================

Simple Plymouth boot splash theme with password support. Looks similar to Windows 10 with VeraCrypt

![Kreelista Plymouth Theme preview](preview.webm)

Copyright (c) 2021-2022, Ofee Oficsu <oficsu@gmail.com> \
Copyright (c) 2018, Mauro A. Meloni <maumeloni@gmail.com>

Based on

- Debian à la Windows 10 boot splash theme for Plymouth
  https://gitlab.com/maurom/deb10

- Pure CSS Windows10 Loader by Fernando de Almeida Faria
  https://codepen.io/feebaa/pen/PPrLQP

- Plymouth theming guide (part 4)
  http://brej.org/blog/?p=238

Documentation about scripting available at
- https://www.freedesktop.org/wiki/Software/Plymouth/Scripts/

- https://sources.debian.org/src/plymouth/0.9.2-4/src/plugins/splash/script/

### Installation

```bash
wget https://gitlab.com/oficsu/kreelista/-/archive/master/kreelista-master.tar.gz
tar xvaf kreelista-master.tar.gz
cd kreelista-master
# install with debian logo
make install # as root or add sudo
# install with your native boot logo
make install-bgrt # as root or add sudo
make apply-now
# reboot to see the new theme
```

### License

This theme is licensed under GPLv2, for more details check COPYING.
