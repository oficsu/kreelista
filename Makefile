.PHONY: install uninstall set-default install-bgrt

themes-path = /usr/share/plymouth/themes
kreelista-path = $(themes-path)/kreelista

uninstall:
	rm -rv $(kreelista-path) >/dev/null 2>&1 || true

install: uninstall
	mkdir $(kreelista-path)
	cp -v *.script *.plymouth *.png $(kreelista-path)/

install-bgrt: install
	cp -v /sys/firmware/acpi/bgrt/image $(kreelista-path)/logo.png

apply: install
ifneq ($(shell which plymouth-set-default-theme),)
	plymouth-set-default-theme kreelista
else ifneq ($(shell which update-alternatives),)
	update-alternatives --install $(themes-path)/default.plymouth default.plymouth $(kreelista-path)/kreelista.plymouth 10
	update-alternatives --set default.plymouth $(kreelista-path)/kreelista.plymouth
else
	$(error "neither plymouth-set-default-theme nor update-alternatives is in PATH")
endif

apply-now: apply
ifneq ($(shell which plymouth-set-default-theme),)
	plymouth-set-default-theme -R kreelista || true
else ifneq ($(shell which update-alternatives),)
	update-initramfs -u
endif

set-default: apply-now
	$(warning warning: target 'set-default' is deprecated, use 'apply-now' instead)
